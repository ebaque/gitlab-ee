# frozen_string_literal: true

class Analytics::ApplicationController < ApplicationController
  include RoutableActions

  layout 'analytics'
end
